import { Select } from "antd";

const SelectField = (props) => {
  const {
    options,
    handleSelect,
    defaultValue,
    loading,
    fieldNames,
    handleClear,
    handleSearch,
  } = props;

  return (
    <Select
      allowClear
      showSearch
      onClear={handleClear}
      fieldNames={fieldNames}
      loading={loading}
      onSelect={handleSelect}
      defaultValue={defaultValue}
      placeholder="Search to Select"
      options={options}
      onSearch={handleSearch}
      filterOption={false}
    />
  );
};

export default SelectField;
