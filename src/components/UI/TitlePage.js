import React from "react";
import { Typography } from "antd";

function TitlePage({ children }) {
  return <Typography.Title style={{ margin: 0 }}>{children}</Typography.Title>;
}

export default TitlePage;
