import { HddOutlined } from "@ant-design/icons";
import { Col, Input, Row, Table, Typography } from "antd";
import React from "react";

function TableSearch({ children, ...props } = {}) {
  const onChangePage = () => {};
  const size = 10;
  return (
    <Col className="wrapper-white table-view">
      <div>
        {children ? (
          <Row style={{ padding: "10px 0" }}>
            <Col {...props.rowFilter?.firstColumn}>{children}</Col>
            <Col {...props.rowFilter?.secondColumn}>
              <Input.Search allowClear onSearch={props.onSearch} />
            </Col>
          </Row>
        ) : (
          <Row>
            <Col flex="auto" style={{ padding: "10px 0" }}>
              <Input.Search allowClear onSearch={props.onSearch} />
            </Col>
          </Row>
        )}
      </div>

      <Table
        style={props?.style}
        loading={props.loading}
        columns={props.columns}
        dataSource={props.dataSource ?? []}
        footer={() => (
          <Typography.Text>
            <HddOutlined /> <strong>{props.totalItems || 0}</strong> data{" "}
            {props?.moduleName}
          </Typography.Text>
        )}
        pagination={{
          position: ["botttomCenter"],
          defaultPageSize: 10,
          current: props.currentPage || 1,
          total: props.totalItems || 0,
          onChange: onChangePage,
          pageSize: size,
          showSizeChanger: true,
        }}
        rowKey="id"
        scroll={props?.scroll}
      />
    </Col>
  );
}

export default TableSearch;
