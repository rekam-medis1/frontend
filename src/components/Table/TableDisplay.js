import { Table, Typography, Button, Input } from "antd";
import { HddOutlined, SyncOutlined } from "@ant-design/icons";
import React, { useState } from "react";

function TableDisplay(props) {
  const [size, setPageSize] = useState(props.limit);

  const onChangePage = (currentPage, limit) => {
    setPageSize(limit);
    props.onChangePage(currentPage, limit);
  };

  return (
    <Table
      title={() => (
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "flex-end",
            gap: 20,
          }}
        >
          {props?.addSearch && (
            <Input.Search
              allowClear
              placeholder={props?.searchPlaceholder}
              onSearch={props?.onSearch}
            />
          )}
          {props.refreshButton && (
            <Button icon={<SyncOutlined />} onClick={props.handleRefresh}>
              Refresh
            </Button>
          )}
        </div>
      )}
      bordered={props?.bordered}
      style={props?.style}
      loading={props.loading}
      columns={props.columns}
      dataSource={props.dataSource ?? []}
      footer={() =>
        props.totalItems && (
          <Typography.Text>
            <HddOutlined /> <strong>{props.totalItems || 0}</strong> data{" "}
            {props?.moduleName}
          </Typography.Text>
        )
      }
      pagination={
        props.pagination
          ? {
              position: ["topRight"],
              defaultPageSize: 10,
              current: props.currentPage || 1,
              total: props.totalItems || 0,
              onChange: onChangePage,
              pageSize: size,
              showSizeChanger: true,
            }
          : false
      }
      rowKey="id"
      scroll={props?.scroll}
    />
  );
}

export default TableDisplay;
