import { LogoutOutlined } from "@ant-design/icons";

const items = [
  // {
  //   key: "1",
  //   label: "1st menu item",
  // },
  // {
  //   key: "2",
  //   label: "2nd menu item",
  // },
  // {
  //   type: "divider",
  // },
  {
    key: "signOut",
    danger: true,
    icon: <LogoutOutlined />,
    label: "Log Out",
  },
];

export { items };
