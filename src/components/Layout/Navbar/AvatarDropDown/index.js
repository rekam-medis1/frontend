import { UserOutlined } from "@ant-design/icons";
import { Avatar, Dropdown, Space, Typography } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { signOut, userInfo } from "../../../../pages/Login/login.slice";
import { items } from "./avatarDD.items";

export default function AvatarDropDown() {
  const { userProfile } = useSelector(userInfo);
  const dipatch = useDispatch();

  const logout = () => {
    dipatch(signOut());
  };

  return (
    <Dropdown
      menu={{
        items,
        onClick: ({ key }) => {
          if (key === "signOut") {
            logout();
          }
        },
      }}
    >
      <button
        onClick={(e) => e.preventDefault()}
        style={{
          color: "#1A120B",
          border: "none",
          backgroundColor: "transparent",
        }}
      >
        <Space style={{ cursor: "pointer" }}>
          <Avatar icon={<UserOutlined />} />
          <Typography.Text>{userProfile?.data?.user?.username}</Typography.Text>
        </Space>
      </button>
    </Dropdown>
  );
}
