import {
  DatabaseOutlined,
  IdcardOutlined,
  LayoutOutlined,
  TableOutlined,
} from "@ant-design/icons";
import { getItem } from "./sidebarGetItem";
import { paths } from "../../../config";

const sidebarItems = () => {
  const items = [
    getItem("Dashboard", `/${paths.dashboard}`, <LayoutOutlined />),
    getItem("Tabel LDK", `/${paths.checkup}`, <TableOutlined />),
    getItem("Input Data LDK", `/${paths.formLdk}`, <IdcardOutlined />),
    getItem("Master", "sub1", <DatabaseOutlined />, [
      // getItem("User", `/${paths.masterUser}`),
      getItem("Range Usia", `/${paths.masterAgeRange}`),
      // getItem("Puskesmas", `/${paths.masterPuskesmas}`),
      getItem("Tipe Pasien", `/${paths.masterPatientType}`),
      getItem("Kategori Penyakit", `/${paths.masterDiseaseCategory}`),
    ]),
  ];

  return { items };
};

export { sidebarItems };
