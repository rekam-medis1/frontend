import { Button, Form, Input, Radio } from "antd";
import React, { useEffect } from "react";
import { validateNumber } from "../../../utils";

function FormPatient(props) {
  const { initialState = {}, onFinish } = props;
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({ ...initialState });
    // eslint-disable-next-line
  }, [initialState]);

  return (
    <Form
      layout="vertical"
      form={form}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 5 }}
      requiredMark={false}
      onFinish={onFinish}
    >
      <Form.Item
        label="NIK"
        name="nik"
        rules={[
          {
            validator: validateNumber,
            message: `masukan nik dengan benar`,
          },
          {
            required: true,
            message: `masukan nik`,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Nama"
        name="name"
        rules={[
          { required: true, message: `masukan nama` },
          { type: "string", message: "masukan nama" },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="gender"
        label="Jenis Kelamin"
        rules={[{ required: true, message: "pilih jenis kelamin " }]}
      >
        <Radio.Group>
          <Radio value="L">Laki Laki</Radio>
          <Radio value="P">Perempuan</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" type="primary">
          Simpan
        </Button>
      </Form.Item>
    </Form>
  );
}

export default FormPatient;
