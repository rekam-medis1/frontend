import { Space, Button } from "antd";
import { useNavigate } from "react-router-dom";
import { paths } from "../../config";

const PatientColumns = () => {
  const navigate = useNavigate();
  const onNavigateUpdate = (record) => {
    navigate(`/${paths.patient}/update`, { state: record });
  };

  const columns = [
    {
      title: "Nama",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "NIK",
      dataIndex: "nik",
      key: "nik",
    },
    {
      title: "Jenis Kelamin",
      dataIndex: "gender",
      key: "gender",
    },
    {
      title: "Action",
      key: "action",
      width: 200,
      ellipsis: "right",
      render: (_, record) => {
        return (
          <Space>
            <Button
              onClick={() => {
                onNavigateUpdate(record);
              }}
              size="middle"
              type="primary"
            >
              Edit
            </Button>
            <Button size="middle" type="primary" danger>
              Delete
            </Button>
          </Space>
        );
      },
    },
  ];

  return { columns };
};

export default PatientColumns;
