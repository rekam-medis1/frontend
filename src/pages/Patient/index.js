import { Button, Col } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";
import { TableView, TitlePage } from "../../components";
import { paths } from "../../config";
import PatientColumns from "./patientColumns";

const Patient = () => {
  const { columns } = PatientColumns();
  const data = [];
  for (let i = 0; i < 100; i++) {
    data.push({
      key: i,
      name: `Edward King ${i}`,
      nik: i + 1,
      gender: i % 2 === 0 ? "L" : "P",
    });
  }

  const navigate = useNavigate();

  return (
    <>
      <Col
        span={24}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <TitlePage>Pasien</TitlePage>
        <Button
          type="primary"
          onClick={() => {
            navigate(`/${paths.patient}/create`);
          }}
        >
          Tambah Pasien
        </Button>
      </Col>
      <Col span={24}>
        <TableView.TableSearch
          key={"itemsPatient"}
          limitSource={[10, 20, 50, 100]}
          limit={10}
          loading={false}
          columns={columns}
          dataSource={data}
          currentPage={1}
          totalItems={100}
          onChangePage={() => {
            console.log("changePage");
          }}
          primaryKey={"patientId"}
          onSearch={() => {
            console.log("searching");
          }}
          moduleName={"pasien"}
          rowFilter={{ firstColumn: { md: 1 }, secondColumn: { md: 23 } }}
          scroll={{ y: 250 }}
        />
      </Col>
    </>
  );
};

export default Patient;
