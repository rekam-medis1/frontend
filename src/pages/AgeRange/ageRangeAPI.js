import { endpoints, request } from "../../utils";

export function getAgeRangeAPI(payload) {
  return request.get(endpoints.masterAgeRange, payload);
}

export function postAgeRangeAPI(payload) {
  return request.post(endpoints.masterAgeRange, payload);
}

export function putAgeRangeAPI(payload, id) {
  return request.put(`${endpoints.masterAgeRange}/${id}`, payload);
}

export function deleteAgeRangeAPI(id) {
  return request.delete(`${endpoints.masterAgeRange}/${id}`);
}
