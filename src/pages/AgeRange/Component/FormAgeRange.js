import { Button, Form, Input, Radio } from "antd";
import React, { useEffect } from "react";
import { validateNumber, splitAgeName } from "../../../utils";

function FormAgeRange(props) {
  const { initialState = {}, onFinish } = props;
  const [form] = Form.useForm();

  useEffect(() => {
    const fieldRangeVallue = splitAgeName(initialState?.name);
    form.setFieldsValue(fieldRangeVallue);
  }, [initialState, form]);

  return (
    <Form
      layout="vertical"
      form={form}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 10 }}
      requiredMark={false}
      onFinish={onFinish}
    >
      <Form.Item label="Range Usia" style={{ marginBottom: 0 }}>
        <Form.Item
          name="firstAge"
          rules={[
            { required: true, message: `masukan umur` },
            { validator: validateNumber, message: "masukan umur dengan benar" },
          ]}
          style={{
            display: "inline-block",
            width: "calc(50% - 12px)",
            margin: 0,
          }}
        >
          <Input />
        </Form.Item>
        <span
          style={{
            display: "inline-block",
            width: "24px",
            lineHeight: "32px",
            textAlign: "center",
          }}
        >
          -
        </span>
        <Form.Item
          name="secondAge"
          rules={[
            { required: true, message: `masukan umur` },
            { validator: validateNumber, message: "masukan umur dengan benar" },
          ]}
          style={{
            display: "inline-block",
            width: "calc(50% - 12px)",
            margin: 0,
          }}
        >
          <Input />
        </Form.Item>
      </Form.Item>
      <Form.Item
        label="Satuan Waktu"
        name="time"
        rules={[{ required: true, message: "pilih jenis waktu " }]}
      >
        <Radio.Group>
          <Radio value="Tahun">Tahun</Radio>
          <Radio value="Hari">Hari</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" type="primary">
          Simpan
        </Button>
      </Form.Item>
    </Form>
  );
}

export default FormAgeRange;
