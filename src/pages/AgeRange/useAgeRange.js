import { message } from "antd";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { paths } from "../../config";
import {
  getAgeRangeAPI,
  postAgeRangeAPI,
  putAgeRangeAPI,
  deleteAgeRangeAPI,
} from "./ageRangeAPI";

const useAgeRange = () => {
  const [ageRange, setAgeRange] = useState({ loading: true, data: null });
  const [query, setQuery] = useState(true);

  const getList = async () => {
    try {
      setAgeRange({ ...ageRange, loading: true });
      const { data } = await getAgeRangeAPI();

      setAgeRange({ ...ageRange, loading: false, data });
    } catch (error) {
      message.error(error.message);
    }
  };

  const reRender = () => {
    setQuery(!query);
  };

  useEffect(() => {
    getList();
    setQuery(query);
    // eslint-disable-next-line
  }, [query]);

  return { ageRange, getList, setAgeRange, reRender, setQuery, query };
};

const useStoredAgeRange = () => {
  const navigate = useNavigate();

  const createAgeRange = async (payload) => {
    try {
      // eslint-disable-next-line
      const { data } = await postAgeRangeAPI(payload);
      message.info("success create age range");
      navigate(`/${paths.masterAgeRange}`);
    } catch (error) {
      message.error(error.message);
    }
  };

  const updatAgeRange = async (payload, id) => {
    try {
      // eslint-disable-next-line
      const { data } = await putAgeRangeAPI(payload, id);
      message.info("success update age range");
      navigate(`/${paths.masterAgeRange}`);
    } catch (error) {
      message.error(error.message);
    }
  };

  const deleteAgeRange = async (id, callback) => {
    try {
      // eslint-disable-next-line
      const { data } = await deleteAgeRangeAPI(id);
      message.info("success delete age range");
      callback();
    } catch (error) {
      message.error(error.message);
    }
  };

  return { createAgeRange, updatAgeRange, deleteAgeRange };
};

export { useAgeRange, useStoredAgeRange };
