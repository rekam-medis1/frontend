import { Button, Col } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";
import { TableView, TitlePage } from "../../components";
import { paths } from "../../config";
import AgeRangeColumns from "./ageRangeColumns";
import { useAgeRange, useStoredAgeRange } from "./useAgeRange";

const AgeRange = () => {
  const { ageRange, reRender } = useAgeRange();
  const { deleteAgeRange } = useStoredAgeRange();

  const destroy = (id) => {
    deleteAgeRange(id, reRender);
  };

  const { columns } = AgeRangeColumns({ destroy });
  const navigate = useNavigate();

  return (
    <>
      <Col
        span={24}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <TitlePage>Master Range Usia </TitlePage>
        <Button
          type="primary"
          onClick={() => {
            navigate(`/${paths.masterAgeRange}/create`);
          }}
        >
          Tambah Range Usia
        </Button>
      </Col>
      <Col span={24}>
        <TableView.TableDisplay
          key={"itemsPatient"}
          limitSource={[10, 20, 50, 100]}
          limit={10}
          loading={ageRange?.loading}
          columns={columns}
          dataSource={ageRange?.data?.ageRange}
          currentPage={1}
          totalItems={ageRange?.data?.ageRange?.length}
          primaryKey={"ageRangeId"}
          moduleName={"Range Umur"}
          rowFilter={{ firstColumn: { md: 1 }, secondColumn: { md: 23 } }}
          scroll={{ y: 250 }}
        />
      </Col>
    </>
  );
};

export default AgeRange;
