import { Col, Row } from "antd";
import React from "react";
import { useLocation, useParams } from "react-router-dom";
import { TitlePage } from "../../../components";
import { FormAgeRange } from "../Component";
import { useStoredAgeRange } from "../useAgeRange";

function PatientCreate() {
  const location = useLocation();
  const params = useParams();
  const action = params.action.split("-")[0];

  const { createAgeRange, updatAgeRange } = useStoredAgeRange();

  const onStoreData = (data) => {
    const { firstAge, secondAge, time } = data;
    const value = { name: `${firstAge} - ${secondAge} ${time}` };

    if (action === "update") {
      updatAgeRange(value, location.state.id);
    } else {
      createAgeRange(value);
    }
  };

  return (
    <>
      <Row className="wrapper-form">
        <Col span={24} style={{ marginBottom: 20 }}>
          <TitlePage>
            {action === `create` ? "Tambah" : "Edit"} Range Usia
          </TitlePage>
        </Col>
        <Col span={24}>
          <FormAgeRange initialState={location.state} onFinish={onStoreData} />
        </Col>
      </Row>
    </>
  );
}

export default PatientCreate;
