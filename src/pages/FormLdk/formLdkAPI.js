import { endpoints, request } from "../../utils";

export function getListDiseaseCategoryAPI(payload) {
  return request.get(
    `${endpoints.masterDiseaseCategory}/children?code=${payload.search}`
  );
}

export function getListAgeRangeAPI(payload) {
  return request.get(`${endpoints.masterAgeRange}?name=${payload.search}`);
}

export function getListPatientTypeAPI(payload) {
  return request.get(`${endpoints.masterPatientType}?search=${payload.search}`);
}

export function postCheckupAPI(payload) {
  return request.post(endpoints.checkup, payload);
}
