import { message } from "antd";
import { useEffect, useState } from "react";
import {
  getListAgeRangeAPI,
  getListDiseaseCategoryAPI,
  getListPatientTypeAPI,
} from "./formLdkAPI";

const useSelectDiseaseCategory = () => {
  const [params, setParams] = useState({ search: "" });
  const [diseaseCategory, setDiseaseCategory] = useState({
    loading: false,
    data: null,
  });

  const getList = async (payload) => {
    try {
      setDiseaseCategory({ ...diseaseCategory, loading: true });
      const { data } = await getListDiseaseCategoryAPI(payload);
      setDiseaseCategory({ loading: false, data });
    } catch (error) {
      message.error(error.message);
    }
  };

  const onSearchDisease = (value) => setParams({ search: value });

  useEffect(() => {
    getList(params);
    // eslint-disable-next-line
  }, [params]);

  return { diseaseCategory, onSearchDisease };
};

const useSelectAgeRange = () => {
  const [params, setParams] = useState({ search: "" });
  const [ageRange, setAgeRange] = useState({
    loading: false,
    data: null,
  });

  const getList = async (payload) => {
    try {
      setAgeRange({ ...ageRange, loading: true });
      const { data } = await getListAgeRangeAPI(payload);
      setAgeRange({ loading: false, data });
    } catch (error) {
      message.error(error.message);
    }
  };

  const onSearchAgeRange = (value) => setParams({ search: value });

  useEffect(() => {
    getList(params);
    // eslint-disable-next-line
  }, [params]);

  return { ageRange, onSearchAgeRange };
};

const useSelectPatientType = () => {
  const [params, setParams] = useState({ search: "" });
  const [patientType, setPatientType] = useState({
    loading: false,
    data: null,
  });

  const getList = async (payload) => {
    try {
      setPatientType({ ...patientType, loading: true });
      const { data } = await getListPatientTypeAPI(payload);
      setPatientType({ loading: false, data });
    } catch (error) {
      message.error(error.message);
    }
  };

  const onSearchPatientType = (value) =>
    setParams({ ...params, search: value });

  useEffect(() => {
    getList(params);
    // eslint-disable-next-line
  }, [params]);

  return { patientType, onSearchPatientType };
};

export { useSelectAgeRange, useSelectDiseaseCategory, useSelectPatientType };
