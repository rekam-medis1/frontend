import { Col, Row } from "antd";
import React from "react";
import { TitlePage } from "../../components";
import { FormInputLdk } from "./Component";

function PuskesmasCreate() {
  const onStoreData = (data) => {
    console.log(data);
  };

  return (
    <>
      <Row className="wrapper-form">
        <Col span={24} style={{ marginBottom: 20 }}>
          <TitlePage>Form LDK</TitlePage>
        </Col>
        <Col span={24}>
          <FormInputLdk onFinish={onStoreData} />
        </Col>
      </Row>
    </>
  );
}

export default PuskesmasCreate;
