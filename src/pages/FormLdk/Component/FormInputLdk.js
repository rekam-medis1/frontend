import { Button, Form, InputNumber, message, Radio } from "antd";
import React, { useState } from "react";
import { SelectField } from "../../../components";
import { postCheckupAPI } from "../formLdkAPI";
import {
  useSelectAgeRange,
  useSelectDiseaseCategory,
  useSelectPatientType,
} from "../useSelectForm";

function FormInputLdk(props) {
  // const { onFinish } = props;
  const [form] = Form.useForm();
  const { ageRange, onSearchAgeRange } = useSelectAgeRange();
  const { diseaseCategory, onSearchDisease } = useSelectDiseaseCategory();
  const { patientType, onSearchPatientType } = useSelectPatientType();
  const [loading, setLoading] = useState(false);

  const handleSelectDiseaseCategory = (value) => {
    form.setFieldsValue({ diseaseCategoryId: value });
  };

  const handleSelectAgeRange = (value) => {
    form.setFieldsValue({ ageRangeId: value });
  };

  const handleSelectPatientType = (value) => {
    form.setFieldsValue({ patientTypeId: value });
  };

  const handleClear = () => {
    form.resetFields();
  };

  const onFinish = async (values) => {
    setLoading(true);

    try {
      await postCheckupAPI(values);
      message.info("input data success");
      form.resetFields();
    } catch (error) {
      message.error("input data failed");
    }

    setLoading(false);
  };

  return (
    <Form
      layout="vertical"
      form={form}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 5 }}
      requiredMark={false}
      onFinish={onFinish}
    >
      <Form.Item
        label="Kode Diagnosa"
        name="diseaseCategoryId"
        rules={[{ required: true, message: "pilih Kode " }]}
      >
        <SelectField
          handleSelect={handleSelectDiseaseCategory}
          options={diseaseCategory.data?.diseaseCategory}
          fieldNames={{ label: "code", value: "id" }}
          loading={diseaseCategory.loading}
          handleClear={handleClear}
          handleSearch={onSearchDisease}
        />
      </Form.Item>
      <Form.Item
        label="Range Usia"
        name="ageRangeId"
        rules={[{ required: true, message: "pilih range usia " }]}
      >
        <SelectField
          handleSelect={handleSelectAgeRange}
          options={ageRange.data?.ageRange}
          fieldNames={{ label: "name", value: "id" }}
          loading={ageRange.loading}
          handleClear={handleClear}
          handleSearch={onSearchAgeRange}
        />
      </Form.Item>
      <Form.Item
        label="Tipe Pasien"
        name="patientTypeId"
        rules={[{ required: true, message: "pilih tipe pasien " }]}
      >
        <SelectField
          handleSelect={handleSelectPatientType}
          options={patientType.data?.patientType}
          fieldNames={{ label: "name", value: "id" }}
          loading={patientType.loading}
          handleClear={handleClear}
          handleSearch={onSearchPatientType}
        />
      </Form.Item>
      <Form.Item
        name="gender"
        label="Jenis Kelamin"
        rules={[{ required: true, message: "pilih jenis kelamin " }]}
      >
        <Radio.Group>
          <Radio value="L">Laki Laki</Radio>
          <Radio value="P">Perempuan</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item
        name="amount"
        label="Jumlah"
        rules={[{ required: true, message: "masukan jumlah pasien" }]}
      >
        <InputNumber />
      </Form.Item>
      <Form.Item>
        <Button loading={loading} htmlType="submit" type="primary">
          Simpan
        </Button>
      </Form.Item>
    </Form>
  );
}

export default FormInputLdk;
