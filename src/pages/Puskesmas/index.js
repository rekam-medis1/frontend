import { Button, Col } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";
import { TableView, TitlePage } from "../../components";
import { paths } from "../../config";
import PuskesmasColumns from "./puskesmasColumns";
import { usePuskesmas } from "./usePuskesmas";

const Puskesmas = () => {
  const { columns } = PuskesmasColumns();
  const navigate = useNavigate();
  const { puskesmas, setQuery } = usePuskesmas();

  const handleRefresh = () => {
    setQuery((query) => !query);
  };

  return (
    <>
      <Col
        span={24}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <TitlePage>Master Data Puskesmas</TitlePage>
        <Button
          type="primary"
          onClick={() => {
            navigate(`/${paths.masterPuskesmas}/create`);
          }}
        >
          Tambah Puskesmas
        </Button>
      </Col>
      <Col span={24}>
        <TableView.TableDisplay
          handleRefresh={handleRefresh}
          key={"itemsUser"}
          limitSource={[10, 20, 50, 100]}
          limit={10}
          loading={false}
          columns={columns}
          dataSource={puskesmas.data?.puskesmas}
          currentPage={1}
          totalItems={100}
          onChangePage={() => {
            console.log("changePage");
          }}
          primaryKey={"UserId"}
          onSearch={() => {
            console.log("searching");
          }}
          moduleName={"user"}
          rowFilter={{ firstColumn: { md: 1 }, secondColumn: { md: 23 } }}
          scroll={{ y: 250 }}
        />
      </Col>
    </>
  );
};

export default Puskesmas;
