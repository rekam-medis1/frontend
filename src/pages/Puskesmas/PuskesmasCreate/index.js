import { Col, Row } from "antd";
import React from "react";
import { useLocation, useParams } from "react-router-dom";
import { TitlePage } from "../../../components";
import { FormPuskesmas } from "../Component";
import { useStoredPuskesmas } from "../usePuskesmas";

function PuskesmasCreate() {
  const location = useLocation();
  const params = useParams();
  const action = params.action.split("-")[0];
  const { createPuskesmas, updatePuskesmas } = useStoredPuskesmas();

  const onStoreData = (data) => {
    if (action === "create") {
      createPuskesmas(data);
    } else {
      updatePuskesmas(data, location.state.id);
    }
  };

  return (
    <>
      <Row className="wrapper-form">
        <Col span={24} style={{ marginBottom: 20 }}>
          <TitlePage>
            {action === `create` ? "Tambah" : "Edit"} Puskesmas
          </TitlePage>
        </Col>
        <Col span={24}>
          <FormPuskesmas initialState={location.state} onFinish={onStoreData} />
        </Col>
      </Row>
    </>
  );
}

export default PuskesmasCreate;
