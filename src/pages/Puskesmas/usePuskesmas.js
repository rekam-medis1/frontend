import { message } from "antd";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { paths } from "../../config";
import {
  deletePuskesmasAPI,
  getPuskesmasAPI,
  postPuskesmasAPI,
  putPuskesmasAPI,
} from "./puskesmasAPI";

const usePuskesmas = () => {
  const [puskesmas, setPuskesmas] = useState({ loading: false, data: null });
  const [query, setQuery] = useState(true);

  const getList = async (params) => {
    try {
      setPuskesmas({ ...puskesmas, loading: true });
      const { data } = await getPuskesmasAPI(params);
      setPuskesmas({ ...puskesmas, loading: false, data });
    } catch (error) {
      message.error(error.message);
    }
  };
  useEffect(() => {
    getList();
    setQuery(query);
    // eslint-disable-next-line
  }, [query]);

  return { puskesmas, setQuery };
};

const useStoredPuskesmas = () => {
  const navigate = useNavigate();

  const createPuskesmas = async (payload) => {
    try {
      // eslint-disable-next-line
      const { data } = await postPuskesmasAPI(payload);
      message.info("create puskesmas success");
      navigate(`/${paths.masterPuskesmas}`);
    } catch (error) {
      message.error(error.message);
    }
  };

  const updatePuskesmas = async (payload, id) => {
    try {
      // eslint-disable-next-line
      const { data } = await putPuskesmasAPI(payload, id);
      message.info("update puskesmas success");
      navigate(`/${paths.masterPuskesmas}`);
    } catch (error) {
      message.error(error.message);
    }
  };

  const deletePuskesmas = async (payload, id) => {
    try {
      // eslint-disable-next-line
      const { data } = await deletePuskesmasAPI(payload, id);
      message.info("delete puskesmas success");
    } catch (error) {
      message.error(error.message);
    }
  };

  return { createPuskesmas, updatePuskesmas, deletePuskesmas };
};

export { usePuskesmas, useStoredPuskesmas };
