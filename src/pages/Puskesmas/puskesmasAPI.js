import { request, endpoints } from "../../utils";

export function getPuskesmasAPI(payload) {
  return request.get(endpoints.masterPuskesmas, payload);
}

export function postPuskesmasAPI(payload) {
  return request.post(endpoints.masterPuskesmas, payload);
}

export function putPuskesmasAPI(payload, id) {
  return request.put(`${endpoints.masterPuskesmas}/${id}`, payload);
}

export function deletePuskesmasAPI(id) {
  return request.delete(`${endpoints.masterPuskesmas}/${id}`);
}
