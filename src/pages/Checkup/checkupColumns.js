import { useColumn } from "./useColumns";

const CheckupColumns = () => {
  // const { column } = useColumn();
  // const columns = [
  //   {
  //     title: "Kode DX",
  //     dataIndex: "codeDx",
  //     key: "codeDx",
  //     width: 80,
  //     fixed: "left",
  //   },
  //   {
  //     title: "Kode ICDX",
  //     dataIndex: "codeIcdx",
  //     key: "codeIcdx",
  //     width: 80,
  //     fixed: "left",
  //   },
  //   {
  //     title: "Nama Penyakit",
  //     dataIndex: "name",
  //     key: "name",
  //     width: 200,
  //     fixed: "left",
  //   },
  //   {
  //     title: "Jumlah Penderita Menurut Golongan Umur",
  //     children: column?.data?.columns,
  //   },
  // ];
  // return { columns, loading: column.loading };

  const columns = [
    {
      title: "Kode ICDX",
      dataIndex: "disease_category_code",
      key: "disease_category_code",
      width: 100,
      fixed: "left",
      render: (_, record) => {
        return record.disease_category.code;
      },
    },
    {
      title: "Diagnosa",
      dataIndex: "disease_category_name",
      key: "disease_category_name",
      ellipsis: true,
      render: (_, record) => record.disease_category.name,
    },
    {
      title: "Range Usia",
      dataIndex: "age_range",
      key: "age_range",
      width: 150,
      render: (_, record) => record.age_range.name,
    },
    {
      title: "Tipe",
      dataIndex: "patient_type",
      key: "patient_type",
      width: 200,
      render: (_, record) => record.patient_type.name,
    },
    {
      title: "Jenis Kelamin",
      dataIndex: "gender",
      width: 200,
      key: "gender",
    },
    {
      title: "Jumlah",
      dataIndex: "amount",
      width: 100,
      key: "amount",
    },
  ];

  return { columns };
};

export default CheckupColumns;
