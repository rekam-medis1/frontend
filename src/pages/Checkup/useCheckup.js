import { message } from "antd";
import { useEffect, useState } from "react";
import { deleteCheckupAPI, getCheckupAPI } from "./checkupAPI";

const useCheckup = () => {
  const [checkup, setCheckup] = useState({ loading: false, data: null });
  // const [params, setParams] = useState({ code: "", page: 1, limit: 10 });

  const getList = async () => {
    try {
      setCheckup({ ...checkup, loading: true });
      const { data } = await getCheckupAPI();
      setCheckup({ loading: false, data });
    } catch (error) {
      message.error(error.message);
      setCheckup({ loading: false, data: null });
    }
  };

  // const onSearchCheckup = (value) => setParams({ ...params, code: value });
  // const onChangePage = (page, limit) => setParams({ ...params, page, limit });
  const onDelete = async (callback) => {
    try {
      setCheckup({ loading: true, ...checkup });
      const { data } = await deleteCheckupAPI();
      setCheckup({ loading: false, data });
      callback();
    } catch (error) {
      message.error(error.message);
    }
  };

  useEffect(() => {
    getList();
    // eslint-disable-next-line
  }, []);

  return { checkup, onDelete, getList };
};

export { useCheckup };
