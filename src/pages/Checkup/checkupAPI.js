import { endpoints, request } from "../../utils";

export const getColumnAPI = () => {
  return request.get(endpoints.column);
};

export const getCheckupAPI = () => {
  return request.get(`${endpoints.checkup}`);
};

export const deleteCheckupAPI = () => {
  return request.delete(endpoints.checkup);
};
