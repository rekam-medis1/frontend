import { ExclamationCircleFilled } from "@ant-design/icons";
import { Button, Col, Modal, Space } from "antd";
import { Excel } from "antd-table-saveas-excel";
import React from "react";
import { TableView, TitlePage } from "../../components";
import { useCheckup } from "./useCheckup";

const Checkup = () => {
  const { checkup, onDelete, getList } = useCheckup();
  const excel = new Excel();

  const handleDelete = () => {
    Modal.confirm({
      title: "Apakah anda yakin ingin menghapus semua data?",
      icon: <ExclamationCircleFilled />,
      content: "Data yang terhapus tidak dapat di kembalikan lagi.",
      okText: "Ya",
      okType: "danger",
      cancelText: "Tidak",
      onOk() {
        onDelete(getList);
      },
    });
  };

  const handleDownload = () => {
    excel
      .addSheet("sheet 1")
      .addColumns(checkup.data?.columns)
      .addDataSource(checkup.data?.datasource)
      .saveAs("LDK.xlsx", "blob", true);
  };

  return (
    <>
      <Col
        span={24}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <TitlePage>Table Laporan Data Kesakitan</TitlePage>
        <Space>
          <Button type="primary" onClick={handleDownload}>
            Cetak Laporan
          </Button>
          <Button type="primary" danger onClick={handleDelete}>
            Hapus Data
          </Button>
        </Space>
      </Col>
      <Col span={24}>
        <TableView.TableDisplay
          key={"checkup"}
          limit={1000}
          bordered={true}
          loading={checkup.loading}
          columns={checkup.data?.columns}
          dataSource={checkup.data?.datasource}
          currentPage={checkup.data?.currentPage}
          searchPlaceholder="Masukan Kode ICDX"
          scroll={{ y: 300 }}
        />
      </Col>
    </>
  );
};

export default Checkup;
