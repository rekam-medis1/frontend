import { message } from "antd";
import { useEffect, useState } from "react";
import { getDataDashboardAPI } from "./dashboardAPI";

const useDashboard = () => {
  const [dashboard, setDashboard] = useState({
    loading: false,
    data: null,
  });

  const getList = async () => {
    try {
      setDashboard({ ...dashboard, loading: true });
      const { data } = await getDataDashboardAPI();
      setDashboard({ ...dashboard, loading: false, data });
    } catch (error) {
      message.error(error.message);
    }
  };
  useEffect(() => {
    getList();
    // eslint-disable-next-line
  }, []);

  return { dashboard };
};

export { useDashboard };
