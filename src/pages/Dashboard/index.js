import { Card, Row, Col, Typography, Spin } from "antd";
import React from "react";
import { useDashboard } from "./useDashboard";

export default function Dashboard() {
  const { dashboard } = useDashboard();

  return (
    <>
      <Row>
        <Col>
          <Card style={{ textAlign: "center" }}>
            <Typography.Title style={{ margin: 0, padding: 0 }} level={5}>
              Total Diagnosa Terdata
            </Typography.Title>
            <Typography.Title style={{ margin: 0 }} level={1} type="success">
              {dashboard.loading ? (
                <Spin />
              ) : dashboard.data?.amount ? (
                dashboard.data?.amount
              ) : (
                0
              )}
            </Typography.Title>
          </Card>
        </Col>
      </Row>
    </>
  );
}
