import { endpoints, request } from "../../utils";

export function getDataDashboardAPI() {
  return request.get(endpoints.dashboard);
}
