import { createSlice } from "@reduxjs/toolkit";
import { message } from "antd";
import { signInAPI, getAuthInfoAPI } from "./login.api";
import { GLOBAL_CONST } from "../../constant";
import { cookies } from "../../config";

const loginSlice = createSlice({
  name: GLOBAL_CONST.AUTH_KEY_REDUX,
  initialState: {
    data: null,
    userProfile: null,
    error: null,
    validateUserLoader: false,
    isAuthenticated: !!cookies.getToken(),
  },

  reducers: {
    userInfoActionReducer: (state) => {
      return { ...state, validateUserLoader: true, error: null };
    },
    userInfoReducer: (state, action) => {
      return {
        ...state,
        userProfile: action.payload,
        validateUserLoader: false,
        isAuthenticated: true,
        error: null,
      };
    },
    signInReducer: (state, action) => {
      return {
        ...state,
        validateUserLoader: true,
        data: action.payload,
        isAuthenticated: true,
        error: null,
      };
    },
    signOutReducer: (state, action) => {
      return {
        ...state,
        isAuthenticated: false,
        validateUserLoader: false,
        error: null,
      };
    },
    errorReducer: (state, action) => {
      return {
        ...state,
        isAuthenticated: false,
        validateUserLoader: false,
        error: "error",
      };
    },
  },
});

const {
  userInfoReducer,
  userInfoActionReducer,
  errorReducer,
  signInReducer,
  signOutReducer,
} = loginSlice.actions;

export const postSignIn = (payload) => async (dispatch) => {
  try {
    const { data } = await signInAPI(payload);
    dispatch(signInReducer(data));
    cookies.setToken(data.token, GLOBAL_CONST.EXPIRES_IN);
  } catch (error) {
    dispatch(errorReducer(error));
  }
};

export const signOut = () => (dispatch) => {
  cookies.removeToken();
  dispatch(signOutReducer());
  message.info("sign out success", 10);
};

export const getUserProfile = () => async (dispatch) => {
  try {
    dispatch(userInfoActionReducer());
    const { data } = await getAuthInfoAPI();
    dispatch(userInfoReducer(data));
  } catch (error) {
    dispatch(errorReducer(error));
  }
};

export const userInfo = (state) => state[GLOBAL_CONST.AUTH_KEY_REDUX];

export default loginSlice.reducer;
