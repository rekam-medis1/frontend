import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Col, Form, Image, Input, Row, Typography } from "antd";
import { useDispatch } from "react-redux";
import { postSignIn } from "./login.slice";

const LoginPage = () => {
  const dispatch = useDispatch();

  const onFinish = (values) => {
    dispatch(postSignIn(values));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("failed : ", errorInfo);
  };

  return (
    <Row
      style={{
        height: "100vh",
        width: "100vw",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Row
        className="login-form"
        style={{
          width: 800,
          height: 400,
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Col
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            alignItems: "flex-start",
            height: "100%",
            flexGrow: 1,
            backgroundColor: "whitesmoke",
            padding: 20,
            borderRadius: "20px 0 0 20px",
          }}
        >
          <Typography.Title>
            Selamat Datang <br /> Di Aplikasi Laporan <br />
            Data Kesakitan
          </Typography.Title>
          <Col
            style={{
              gap: "20px",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Image
              src="./logo-pkm-cisadea.png"
              width="80px"
              draggable={false}
              preview={false}
            />
            <Image
              width="80px"
              draggable={false}
              preview={false}
              src="./logo-puskesmas.png"
            />
          </Col>
        </Col>
        <Col
          style={{
            height: "100%",
            alignItems: "center",
            display: "flex",
            backgroundColor: "#ebebeb",
            borderRadius: "0 20px 20px 0",
          }}
        >
          <Col
            style={{
              display: "flex",
              flexDirection: "column",
              height: "70%",
              justifyContent: "space-between",
              padding: 20,
            }}
          >
            <Typography.Title
              style={{ marginTop: 0, textAlign: "center", color: "#1677FF" }}
            >
              Login
            </Typography.Title>
            <Form
              name="normal_login"
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
            >
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: "Please input your Username!",
                  },
                ]}
              >
                <Input
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="Username"
                />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Please input your Password!",
                  },
                ]}
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item style={{ marginBottom: 0 }}>
                <Col>
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="login-form-button"
                    style={{ width: `100%` }}
                  >
                    Log in
                  </Button>
                </Col>
              </Form.Item>
            </Form>
          </Col>
        </Col>
      </Row>
    </Row>
  );
};

export default LoginPage;
