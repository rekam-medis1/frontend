import { endpoints, request } from "../../utils";

export function signInAPI(data) {
  return request.post(endpoints.signIn, data);
}

export function getAuthInfoAPI() {
  return request.post(endpoints.authInfo);
}
