import { Button, Form, Input } from "antd";
import React, { useEffect } from "react";
import { SelectField } from "../../../components";
import { useSelectDiseaseCategory } from "../useSelectDiseaseCategory";

function FormDiseaseCategory(props) {
  const { initialState = {}, onFinish } = props;
  const [form] = Form.useForm();
  const { parent, onSearch } = useSelectDiseaseCategory();

  useEffect(() => {
    form.setFieldsValue({ ...initialState });
    // eslint-disable-next-line
  }, [initialState]);

  const handleSelect = (value) => {
    form.setFieldsValue({ parent: value });
  };

  const handleClear = () => {
    form.setFieldsValue({ parent: null });
  };

  return (
    <Form
      layout="vertical"
      form={form}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 5 }}
      requiredMark={false}
      onFinish={onFinish}
    >
      <Form.Item
        label="Nama"
        name="name"
        rules={[
          { required: true, message: `masukan nama` },
          { type: "string", message: "masukan nama" },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Kode"
        name="code"
        rules={[
          { required: true, message: `masukan kode` },
          { type: "string", message: "masukan kode" },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item label="Kode DX" name="parent">
        <SelectField
          handleSelect={handleSelect}
          options={parent.data?.diseaseCategory}
          fieldNames={{ label: "code", value: "id" }}
          loading={parent.loading}
          defaultValue={initialState?.parent}
          handleClear={handleClear}
          handleSearch={onSearch}
        />
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" type="primary">
          Simpan
        </Button>
      </Form.Item>
    </Form>
  );
}

export default FormDiseaseCategory;
