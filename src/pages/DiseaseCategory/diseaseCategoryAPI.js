import { endpoints, request } from "../../utils";

export function getAllDiseaseCategoryAPI(payload) {
  return request.get(endpoints.masterDiseaseCategory, payload);
}

export function getParentDiseaseCategoryAPI(payload) {
  return request.get(
    `${endpoints.masterDiseaseCategory}/parent?code=${payload.code}`
  );
}

export function getChildrenDiseaseCategoryAPI(payload) {
  return request.get(`${endpoints.masterDiseaseCategory}/children`, payload);
}

export function getNestedDiseaseCategoryAPI(params) {
  return request.get(
    `${endpoints.masterDiseaseCategory}/nested?name=${params?.name}&limit=${params?.limit}&page=${params?.page}`
  );
}

export function postDiseaseCategoryAPI(payload) {
  return request.post(endpoints.masterDiseaseCategory, payload);
}

export function putDiseaseCategoryAPI(payload, id) {
  return request.put(`${endpoints.masterDiseaseCategory}/${id}`, payload);
}

export function deleteDiseaseCategoryAPI(id) {
  return request.delete(`${endpoints.masterDiseaseCategory}/${id}`);
}
