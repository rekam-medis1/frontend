import { message } from "antd";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { paths } from "../../config";
import {
  deleteDiseaseCategoryAPI,
  getNestedDiseaseCategoryAPI,
  postDiseaseCategoryAPI,
  putDiseaseCategoryAPI,
} from "./diseaseCategoryAPI";

const useDiseaseCategory = () => {
  const [query, setQuery] = useState(true);
  const [diseaseCategory, setDiseaseCategory] = useState({
    loading: false,
    data: null,
  });
  const [params, setParams] = useState({ page: 1, limit: 10, name: "" });

  const getList = async (params) => {
    try {
      setDiseaseCategory({ ...diseaseCategory, loading: true });
      const { data } = await getNestedDiseaseCategoryAPI(params);
      setDiseaseCategory({ loading: false, data });
    } catch (error) {
      message.error(error.message);
    }
  };

  const reRender = () => {
    setQuery(!query);
  };

  const onSearch = (value) => setParams({ ...params, name: value });
  const onChangePage = (page, limit) => setParams({ ...params, page, limit });

  useEffect(() => {
    getList(params);
    setQuery(query);
    // eslint-disable-next-line
  }, [query, params]);

  return { diseaseCategory, reRender, setQuery, onSearch, onChangePage };
};

const useStoredDiseaseCategory = () => {
  const navigate = useNavigate();

  const createDiseaseCategory = async (payload) => {
    try {
      // eslint-disable-next-line
      const { data } = await postDiseaseCategoryAPI(payload);
      message.info("success create disease category");
      navigate(`/${paths.masterDiseaseCategory}`);
    } catch (error) {
      message.error(error.message);
    }
  };

  const updateDiseaseCategory = async (payload, id) => {
    try {
      // eslint-disable-next-line
      const { data } = await putDiseaseCategoryAPI(payload, id);
      message.info("success update disease category");
      navigate(`/${paths.masterDiseaseCategory}`);
    } catch (error) {
      message.error(error.message);
    }
  };

  const deleteDiseaseCategory = async (id, callback) => {
    try {
      // eslint-disable-next-line
      const { data } = await deleteDiseaseCategoryAPI(id);
      message.info("success delete disease category");
      callback();
    } catch (error) {
      message.error(error.message);
    }
  };

  return {
    createDiseaseCategory,
    updateDiseaseCategory,
    deleteDiseaseCategory,
  };
};

export { useDiseaseCategory, useStoredDiseaseCategory };
