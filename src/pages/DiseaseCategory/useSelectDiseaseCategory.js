import { message } from "antd";
import { useEffect, useState } from "react";
import { getParentDiseaseCategoryAPI } from "./diseaseCategoryAPI";

const useSelectDiseaseCategory = () => {
  const [parent, setParent] = useState({ loading: false, data: null });
  const [params, setParams] = useState({ code: "" });

  const getParent = async (payload) => {
    try {
      setParent({ ...parent, loading: true });
      const { data } = await getParentDiseaseCategoryAPI(payload);
      setParent({ loading: false, data });
    } catch (error) {
      message.error(error.message);
    }
  };

  const onSearch = (value) => {
    setParams({ ...params, code: value });
  };

  useEffect(() => {
    getParent(params);
    // eslint-disable-next-line
  }, [params]);

  return { parent, onSearch };
};

export { useSelectDiseaseCategory };
