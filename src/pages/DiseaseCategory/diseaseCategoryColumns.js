import { Button, Space } from "antd";
import { useNavigate } from "react-router-dom";
import { showDeleteConfirm } from "../../components";
import { paths } from "../../config";

const DiseaseCategoryColumns = ({ destroy }) => {
  const navigate = useNavigate();
  const onNavigateUpdate = (record) => {
    navigate(`/${paths.masterDiseaseCategory}/update`, { state: record });
  };

  const handleConfirm = (record) => {
    showDeleteConfirm({
      title: record?.name,
      onOk: () => {
        destroy(record?.id);
      },
    });
  };

  const columns = [
    {
      title: "Diagnosa",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Dx",
      dataIndex: "parentName",
      key: "parentName",
      render: (_, record) => (record.parentName ? record.parentName : "-"),
    },
    {
      title: "Kode Diagnosa",
      dataIndex: "code",
      key: "code",
    },
    {
      title: "Action",
      key: "action",
      width: 200,
      ellipsis: "right",
      render: (_, record) => {
        return (
          <Space>
            <Button
              onClick={() => {
                onNavigateUpdate(record);
              }}
              size="middle"
              type="primary"
            >
              Edit
            </Button>

            <Button
              onClick={() => {
                handleConfirm(record);
              }}
              size="middle"
              type="primary"
              danger
            >
              Delete
            </Button>
          </Space>
        );
      },
    },
  ];

  return { columns };
};

export default DiseaseCategoryColumns;
