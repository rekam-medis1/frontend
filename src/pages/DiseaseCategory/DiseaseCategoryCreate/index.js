import { Col, Row } from "antd";
import React from "react";
import { useLocation, useParams } from "react-router-dom";
import { TitlePage } from "../../../components";
import { FormDiseaseCategory } from "../Component";
import { useStoredDiseaseCategory } from "../useDiseaseCategory";

function PuskesmasCreate() {
  const location = useLocation();
  const params = useParams();
  const action = params.action.split("-")[0];

  const { createDiseaseCategory, updateDiseaseCategory } =
    useStoredDiseaseCategory();

  const onStoreData = (data) => {
    if (action === "create") {
      createDiseaseCategory(data);
    } else {
      updateDiseaseCategory(data, location.state?.id);
    }
  };

  return (
    <>
      <Row className="wrapper-form">
        <Col span={24} style={{ marginBottom: 20 }}>
          <TitlePage>
            {action === `create` ? "Tambah" : "Edit"} Kategori Penyakit
          </TitlePage>
        </Col>
        <Col span={24}>
          <FormDiseaseCategory
            initialState={location.state}
            onFinish={onStoreData}
          />
        </Col>
      </Row>
    </>
  );
}

export default PuskesmasCreate;
