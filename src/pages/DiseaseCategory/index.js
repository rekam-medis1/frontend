import { Button, Col } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";
import { TableView, TitlePage } from "../../components";
import { paths } from "../../config";
import DiseaseCategoryColumns from "./diseaseCategoryColumns";
import {
  useDiseaseCategory,
  useStoredDiseaseCategory,
} from "./useDiseaseCategory";

const DiseaseCategory = () => {
  const { diseaseCategory, onSearch, onChangePage, reRender } =
    useDiseaseCategory();
  const { deleteDiseaseCategory } = useStoredDiseaseCategory();

  const destroy = (id) => {
    deleteDiseaseCategory(id, reRender);
  };

  const navigate = useNavigate();
  const { columns } = DiseaseCategoryColumns({ destroy });

  return (
    <>
      <Col
        span={24}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <TitlePage>Master Data Kategori Penyakit</TitlePage>
        <Button
          type="primary"
          onClick={() => {
            navigate(`/${paths.masterDiseaseCategory}/create`);
          }}
        >
          Tambah Kategori
        </Button>
      </Col>
      <Col span={24}>
        <TableView.TableDisplay
          addSearch={true}
          key={"itemsUser"}
          limitSource={[10, 20, 50, 100]}
          limit={10}
          loading={diseaseCategory.loading}
          columns={columns}
          dataSource={diseaseCategory.data?.diseaseNested}
          currentPage={diseaseCategory.data?.currentPage}
          totalItems={diseaseCategory.data?.totalItems}
          onChangePage={onChangePage}
          primaryKey={"UserId"}
          onSearch={onSearch}
          moduleName={"user"}
          rowFilter={{ firstColumn: { md: 1 }, secondColumn: { md: 23 } }}
          scroll={{ y: 250 }}
          searchPlaceholder="Masukan Diagnosa"
          pagination={true}
        />
      </Col>
    </>
  );
};

export default DiseaseCategory;
