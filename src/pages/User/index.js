import { Button, Col } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";
import { TableView, TitlePage } from "../../components";
import { paths } from "../../config";
import UserColumns from "./userColumns";

const User = () => {
  const { columns } = UserColumns();
  const data = [];
  for (let i = 0; i < 4; i++) {
    data.push({
      key: i,
      username: `Edward King ${i}`,
    });
  }

  const navigate = useNavigate();

  return (
    <>
      <Col
        span={24}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <TitlePage>Master Data User</TitlePage>
        <Button
          type="primary"
          onClick={() => {
            navigate(`/${paths.masterUser}/create`);
          }}
        >
          Tambah User
        </Button>
      </Col>
      <Col span={24}>
        <TableView.TableDisplay
          key={"itemsUser"}
          limitSource={[10, 20, 50, 100]}
          limit={10}
          loading={false}
          columns={columns}
          dataSource={data}
          currentPage={1}
          totalItems={100}
          onChangePage={() => {
            console.log("changePage");
          }}
          primaryKey={"UserId"}
          onSearch={() => {
            console.log("searching");
          }}
          moduleName={"user"}
          rowFilter={{ firstColumn: { md: 1 }, secondColumn: { md: 23 } }}
          scroll={{ y: 250 }}
        />
      </Col>
    </>
  );
};

export default User;
