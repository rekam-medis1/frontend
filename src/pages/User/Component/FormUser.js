import { Button, Form, Input } from "antd";
import React, { useEffect } from "react";

function FormPatient(props) {
  const { initialState = {}, onFinish } = props;
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({ ...initialState });
    // eslint-disable-next-line
  }, [initialState]);

  return (
    <Form
      layout="vertical"
      form={form}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 5 }}
      requiredMark={false}
      onFinish={onFinish}
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[
          { required: true, message: `masukan username` },
          { type: "string", message: "masukan username" },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Password"
        name="password"
        rules={[
          { required: true, message: `masukan password` },
          { type: "string", message: "masukan password" },
        ]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item
        label="Confirm Password"
        name="confirmPassword"
        rules={[
          { required: true, message: `masukan konfirmasi password` },
          { type: "string", message: "masukan konfirmasi password" },
        ]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" type="primary">
          Simpan
        </Button>
      </Form.Item>
    </Form>
  );
}

export default FormPatient;
