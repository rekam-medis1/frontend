import { Col, Row } from "antd";
import React from "react";
import { useLocation, useParams } from "react-router-dom";
import { TitlePage } from "../../../components";
import { FormUser } from "../Component";

function PatientCreate() {
  const location = useLocation();
  const params = useParams();
  const action = params.action.split("-")[0];

  const onStoreData = (data) => {
    console.log(data);
  };

  return (
    <>
      <Row className="wrapper-form">
        <Col span={24} style={{ marginBottom: 20 }}>
          <TitlePage>{action === `create` ? "Tambah" : "Edit"} User</TitlePage>
        </Col>
        <Col span={24}>
          <FormUser initialState={location.state} onFinish={onStoreData} />
        </Col>
      </Row>
    </>
  );
}

export default PatientCreate;
