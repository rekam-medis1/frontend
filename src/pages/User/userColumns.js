import { Space, Button } from "antd";
import { useNavigate } from "react-router-dom";
import { paths } from "../../config";

const UserColumns = () => {
  const navigate = useNavigate();
  const onNavigateUpdate = (record) => {
    navigate(`/${paths.masterUser}/update`, { state: record });
  };

  const columns = [
    {
      title: "Username",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "Action",
      key: "action",
      width: 200,
      ellipsis: "right",
      render: (_, record) => {
        return (
          <Space>
            <Button
              onClick={() => {
                onNavigateUpdate(record);
              }}
              size="middle"
              type="primary"
            >
              Edit
            </Button>

            <Button size="middle" type="primary" danger>
              Delete
            </Button>
          </Space>
        );
      },
    },
  ];

  return { columns };
};

export default UserColumns;
