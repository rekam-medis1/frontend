import { request, endpoints } from "../../utils";

export function getPatientTypeAPI(payload) {
  return request.get(endpoints.masterPatientType, payload);
}

export function postPatientTypeAPI(payload) {
  return request.post(endpoints.masterPatientType, payload);
}

export function putPatientTypeAPI(payload, id) {
  return request.put(`${endpoints.masterPatientType}/${id}`, payload);
}

export function deletePatientTypeAPI(id) {
  return request.delete(`${endpoints.masterPatientType}/${id}`);
}
