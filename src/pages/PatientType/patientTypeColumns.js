import { Button, Space } from "antd";
import { useNavigate } from "react-router-dom";
import { showDeleteConfirm } from "../../components";
import { paths } from "../../config";

const PatientTypeColumns = ({ destroy }) => {
  const navigate = useNavigate();
  const onNavigateUpdate = (record) => {
    navigate(`/${paths.masterPatientType}/update`, { state: record });
  };

  const handleConfirm = (record) => {
    showDeleteConfirm({
      title: record?.name,
      onOk: () => {
        destroy(record?.id);
      },
    });
  };

  const columns = [
    {
      title: "Nama",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Action",
      key: "action",
      width: 200,
      ellipsis: "right",
      render: (_, record) => {
        return (
          <Space>
            <Button
              onClick={() => {
                onNavigateUpdate(record);
              }}
              size="middle"
              type="primary"
            >
              Edit
            </Button>

            <Button
              onClick={() => {
                handleConfirm(record);
              }}
              size="middle"
              type="primary"
              danger
            >
              Delete
            </Button>
          </Space>
        );
      },
    },
  ];

  return { columns };
};

export default PatientTypeColumns;
