import { message } from "antd";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { paths } from "../../config";
import {
  deletePatientTypeAPI,
  getPatientTypeAPI,
  postPatientTypeAPI,
  putPatientTypeAPI,
} from "./patienTypeAPI";

const usePatientType = () => {
  const [patientType, setPatientType] = useState({
    loading: false,
    data: null,
  });
  const [query, setQuery] = useState(true);

  const getList = async (params) => {
    try {
      setPatientType({ ...patientType, loading: true });
      const { data } = await getPatientTypeAPI(params);
      setPatientType({ ...patientType, loading: false, data });
    } catch (error) {
      message.error(error.message);
    }
  };

  const reRender = () => {
    setQuery(!query);
  };

  useEffect(() => {
    getList();
    setQuery(query);
    // eslint-disable-next-line
  }, [query]);

  return { patientType, reRender };
};

const useStoredPatientType = () => {
  const navigate = useNavigate();

  const createPatientType = async (payload) => {
    try {
      // eslint-disable-next-line
      const { data } = await postPatientTypeAPI(payload);
      message.info("create patient type success");
      navigate(`/${paths.masterPatientType}`);
    } catch (error) {
      message.error(error.message);
    }
  };

  const updatePatientType = async (payload, id) => {
    try {
      // eslint-disable-next-line
      const { data } = await putPatientTypeAPI(payload, id);
      message.info("update patient type success");
      navigate(`/${paths.masterPatientType}`);
    } catch (error) {
      message.error(error.message);
    }
  };

  const deletePatientType = async (id, callback) => {
    try {
      // eslint-disable-next-line
      const { data } = await deletePatientTypeAPI(id);
      message.info("delete patient type success");
      callback();
    } catch (error) {
      message.error(error.message);
    }
  };

  return { createPatientType, updatePatientType, deletePatientType };
};

export { usePatientType, useStoredPatientType };
