import { Button, Col } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";
import { TableView, TitlePage } from "../../components";
import { paths } from "../../config";
import PatientTypeColumns from "./patientTypeColumns";
import { usePatientType, useStoredPatientType } from "./usePatientType";

const PatientType = () => {
  const { patientType, reRender } = usePatientType();
  const { deletePatientType } = useStoredPatientType();

  const destroy = (id) => {
    deletePatientType(id, reRender);
  };

  const { columns } = PatientTypeColumns({ destroy });
  const navigate = useNavigate();

  return (
    <>
      <Col
        span={24}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <TitlePage>Master Data Tipe Pasien</TitlePage>
        <Button
          type="primary"
          onClick={() => {
            navigate(`/${paths.masterPatientType}/create`);
          }}
        >
          Tambah Tipe Pasien
        </Button>
      </Col>
      <Col span={24}>
        <TableView.TableDisplay
          key={"itemsUser"}
          limitSource={[10, 20, 50, 100]}
          limit={10}
          loading={false}
          columns={columns}
          dataSource={patientType.data?.patientType}
          currentPage={1}
          totalItems={patientType.data?.patientType?.length}
          primaryKey={"UserId"}
          moduleName={"user"}
          rowFilter={{ firstColumn: { md: 1 }, secondColumn: { md: 23 } }}
          scroll={{ y: 250 }}
          pagination={false}
        />
      </Col>
    </>
  );
};

export default PatientType;
