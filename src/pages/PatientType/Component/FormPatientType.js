import { Button, Form, Input } from "antd";
import React, { useEffect } from "react";

function FormPatientType(props) {
  const { initialState = {}, onFinish } = props;
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({ ...initialState });
    // eslint-disable-next-line
  }, [initialState]);

  return (
    <Form
      layout="vertical"
      form={form}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 5 }}
      requiredMark={false}
      onFinish={onFinish}
    >
      <Form.Item
        label="Nama"
        name="name"
        rules={[
          { required: true, message: `masukan nama puskesmas` },
          { type: "string", message: "masukan nama puskesmas" },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" type="primary">
          Simpan
        </Button>
      </Form.Item>
    </Form>
  );
}

export default FormPatientType;
