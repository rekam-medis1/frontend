import { Col, Row } from "antd";
import React from "react";
import { useLocation, useParams } from "react-router-dom";
import { TitlePage } from "../../../components";
import { FormPatientType } from "../Component";
import { useStoredPatientType } from "../usePatientType";

function PatientTypeCreate() {
  const location = useLocation();
  const params = useParams();
  const action = params.action.split("-")[0];

  const { createPatientType, updatePatientType } = useStoredPatientType();

  const onStoreData = (data) => {
    if (action === "create") {
      createPatientType(data);
    } else {
      updatePatientType(data, location.state?.id);
    }
  };

  return (
    <>
      <Row className="wrapper-form">
        <Col span={24} style={{ marginBottom: 20 }}>
          <TitlePage>
            {action === `create` ? "Tambah" : "Edit"} Tipe Pasien
          </TitlePage>
        </Col>
        <Col span={24}>
          <FormPatientType
            initialState={location.state}
            onFinish={onStoreData}
          />
        </Col>
      </Row>
    </>
  );
}

export default PatientTypeCreate;
