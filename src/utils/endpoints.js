const endpoints = {
  authInfo: "/auth",
  signIn: "/auth/signin",
  column: "/column",
  masterAgeRange: "/master/ageRange",
  masterPatient: "/master/patient",
  masterPatientType: "/master/patientType",
  masterPuskesmas: "/master/puskesmas",
  masterDiseaseCategory: "/master/diseaseCategory",
  checkup: "/checkup",
  dashboard: "/dashboard",
  download: "/download",
};

export default endpoints;
