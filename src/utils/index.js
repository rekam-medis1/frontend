export { default as endpoints } from "./endpoints";
export { default as request } from "./request";

const validateNumber = (rule, value, callback) => {
  if (value && isNaN(value)) {
    callback("Please enter a valid number");
  } else {
    callback();
  }
};

const splitAgeName = (name) => {
  if (name) {
    const split = name?.split(" ");
    const data = {
      firstAge: split[0],
      secondAge: split[2],
      time: split[3],
    };
    return data;
  }
  return;
};

const API_URL = `http://localhost:8000`;
export { validateNumber, splitAgeName, API_URL };
