const paths = {
  login: "login",
  dashboard: "dashboard",
  patient: "patient",
  patientAction: "patient/:action",
  formLdk: "formLdk",
  masterUser: "master/user",
  masterUserAction: "master/user/:action",
  masterAgeRange: "master/ageRange",
  masterAgeRangeAction: "master/ageRange/:action",
  masterPuskesmas: "master/puskesmas",
  masterPuskesmasAction: "master/puskesmas/:action",
  masterPatientType: "master/patientType",
  masterPatientTypeAction: "master/patientType/:action",
  masterDiseaseCategory: "master/diseaseCategory",
  masterDiseaseCategoryAction: "master/diseaseCategory/:action",
  checkup: "checkup",
};

export default paths;
