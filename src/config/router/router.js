import { lazy } from "react";
import paths from "./paths";
import PublicRoutes from "./PublicRoutes";
import ProtectedRoutes from "./ProtectedRoutes";

const FormLdk = lazy(() => import("../../pages/FormLdk"));
const Dashboard = lazy(() => import("../../pages/Dashboard"));
const Login = lazy(() => import("../../pages/Login"));
const AgeRange = lazy(() => import("../../pages/AgeRange"));
const DiseaseCategory = lazy(() => import("../../pages/DiseaseCategory"));
const Patient = lazy(() => import("../../pages/Patient"));
const PatientCreate = lazy(() => import("../../pages/Patient/PatientCreate"));
const PatientType = lazy(() => import("../../pages/PatientType"));
const Puskesmas = lazy(() => import("../../pages/Puskesmas"));
const Checkup = lazy(() => import("../../pages/Checkup"));
const User = lazy(() => import("../../pages/User"));
const UserCreate = lazy(() => import("../../pages/User/UserCreate"));
const AgeRangeCreate = lazy(() =>
  import("../../pages/AgeRange/AgeRangeCreate")
);
const PuskesmasCreate = lazy(() =>
  import("../../pages/Puskesmas/PuskesmasCreate")
);
const PatientTypeCreate = lazy(() =>
  import("../../pages/PatientType/PatientTypeCreate")
);
const DiseaseCategoryCreate = lazy(() =>
  import("../../pages/DiseaseCategory/DiseaseCategoryCreate")
);

const routes = [
  {
    path: paths.login,
    element: (
      <PublicRoutes>
        <Login />
      </PublicRoutes>
    ),
  },
  {
    path: paths.dashboard,
    element: (
      <ProtectedRoutes>
        <Dashboard />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.checkup,
    element: (
      <ProtectedRoutes>
        <Checkup />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.formLdk,
    element: (
      <ProtectedRoutes>
        <FormLdk />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.masterAgeRange,
    element: (
      <ProtectedRoutes>
        <AgeRange />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.masterAgeRangeAction,
    element: (
      <ProtectedRoutes>
        <AgeRangeCreate />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.masterDiseaseCategory,
    element: (
      <ProtectedRoutes>
        <DiseaseCategory />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.masterDiseaseCategoryAction,
    element: (
      <ProtectedRoutes>
        <DiseaseCategoryCreate />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.masterPatientType,
    element: (
      <ProtectedRoutes>
        <PatientType />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.masterPatientTypeAction,
    element: (
      <ProtectedRoutes>
        <PatientTypeCreate />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.masterPuskesmas,
    element: (
      <ProtectedRoutes>
        <Puskesmas />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.masterPuskesmasAction,
    element: (
      <ProtectedRoutes>
        <PuskesmasCreate />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.patient,
    element: (
      <ProtectedRoutes>
        <Patient />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.patientAction,
    element: (
      <ProtectedRoutes>
        <PatientCreate />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.masterUser,
    element: (
      <ProtectedRoutes>
        <User />
      </ProtectedRoutes>
    ),
  },
  {
    path: paths.masterUserAction,
    element: (
      <ProtectedRoutes>
        <UserCreate />
      </ProtectedRoutes>
    ),
  },
];

export default routes;
