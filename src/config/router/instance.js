import { Spin } from "antd";
import { Suspense, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, useRoutes } from "react-router-dom";
import { Loading, NotFound } from "../../components";
import { getUserProfile, userInfo } from "../../pages/Login/login.slice";
import routes from "./router";
import { getToken } from "../cookies";

const InitRouter = () => {
  const defaultPages = [
    { path: "/", element: <Navigate to={"/login"} /> },
    {
      path: "*",
      element: <NotFound />,
    },
  ];

  const Component = useRoutes([...routes, ...defaultPages]);
  return (
    <Suspense
      fallback={
        <Loading.Pages>
          <Spin tip="loading page.." size="large" />
        </Loading.Pages>
      }
    >
      {Component}
    </Suspense>
  );
};

function InstanceRoutes() {
  const dispatch = useDispatch();
  const { isAuthenticated, validateUserLoader } = useSelector(userInfo);

  useEffect(() => {
    if (isAuthenticated && !!getToken()) {
      dispatch(getUserProfile());
    }
    // eslint-disable-next-line
  }, [isAuthenticated]);

  if (validateUserLoader) {
    return (
      <Loading.Pages>
        <Spin tip="loading user..." size="large" />
      </Loading.Pages>
    );
  }

  return <InitRouter />;
}

export default InstanceRoutes;
