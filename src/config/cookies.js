import Cookies from "js-cookie";
import { GLOBAL_CONST } from "../constant";

export function getToken() {
  return Cookies.get(GLOBAL_CONST.TOKEN_KEY);
}

export function setToken(token, expires_in) {
  let min_expires_in = expires_in - 120;
  let expires = min_expires_in / (3600 * 24);
  return Cookies.set(GLOBAL_CONST.TOKEN_KEY, token, { expires });
}

export function removeToken() {
  return Cookies.remove(GLOBAL_CONST.TOKEN_KEY);
}
